<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequisitionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requisition', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('project_id')->nullable();
            $table->date('entry_date')->nullable();
            $table->date('slip_received_date')->nullable();
            $table->unsignedBigInteger('sn')->index();
            $table->unsignedBigInteger('material_id')->nullable();
            $table->string('description')->nullable();
            $table->integer('quantity')->nullable();
            $table->string('detail')->nullable();
            $table->date('required_date')->nullable();
            $table->softDeletes();
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('set null');
            $table->foreign('material_id')->references('id')->on('materials')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requisition');
    }
}
