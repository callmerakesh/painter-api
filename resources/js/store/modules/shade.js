import $api from '../../handler/axios';
export const SET_SHADES = 'SET_SHADES';

const state = {
    shades: {}
};

const mutations = {
    [SET_SHADES](status, payload) {
        state.shades = payload
    },

};

const actions = {
    async getSingleShade(context, id) {
        let url = `/shade/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllShades(context) {
        let url =`/shades`;
        let data = await $api.get(url);
        return data.data;
    },
    async addShade(context, formData) {
        let url = `/shade`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteShade(context, id) {
        let url = `/shade/${id}`;
        return await $api.delete(url);
    }
};

const getters  = {
    shades : state => state.shades,
};

export default {
    state,
    mutations,
    actions,
    getters
}
