import errorHandler from "./error";
import $axios from 'axios';
import store from '../store/index';
import router from '../router';

const instance = $axios.create({
    baseURL: window.location.origin + '/api',
    'headers': {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
});

instance.interceptors.request.use(config => {
    return config;
}, error => {
    api.handleError(error);
    return Promise.reject(error);
});

instance.interceptors.response.use(response => {
    return response;
}, error => {
    api.handleError(error);
    return Promise.reject(error);
});

let api ={
    get: function (target, callback) {
        return instance.get(target).then((response) => {
            if(callback)  return this.handleResponse(response, callback, 'get');
            return response.data;
        })
    },

    getWithPayload: function (target, payload, callback) {
        return instance.get(target, {params: payload}).then((response) => {
            this.handleResponse(response, callback, 'get');
        })
    },

    post: function(target, payload, callback) {
        store.commit('BUTTON_LOADER', true);
        return instance.post(target, payload).then((response) => {
            if(callback) return this.handleResponse(response, callback, 'post');
            store.commit('BUTTON_LOADER', false);
            return response.data;
        });
    },

    delete: function (target, callback) {
        return instance.delete(target).then(
            (response) => this.handleResponse(response, callback, 'delete'),
        );
    },

    handleResponse: function (response, callback, requestType) {
        store.commit('CLEAR_VALIDATIONS');
        if(requestType === 'post') {
            store.commit('BUTTON_LOADER', false);
            return response.data;
        }
        if(callback) {
            return callback(response.status, response.data);
        }
    },

    handleError: function(error) {
        let errorCode = error.response.status;
        store.commit('BUTTON_LOADER', false);
        if(errorCode === 403) router.push('/dashboard');
        if(errorCode === 401) location.reload();
        if(errorCode === 422) store.dispatch('setValidation', error);
        errorHandler.errorCodeHandler(errorCode, error.response.data.errors, this);
        return Promise.reject(error);
    }
};

export default api;
