<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\CreateReferralRequest;
use App\Repositories\Referrals\ReferralRepository;
use Illuminate\Http\Request;

class ReferralController extends ApiBaseController
{
    private $referral;

    public function __construct(ReferralRepository $referral)
    {
        $this->referral = $referral;
    }

    public function index(Request $request)

    {
        return $this->sendResponse($this->referral->all($request->all()), 'Referrals fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->referral->find($id), 'Single Referral retrieved successfully');
    }

    public function store(CreateReferralRequest $request)
    {
        return $this->sendResponse($this->referral->store($request->all()),'Referral saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->referral->update($request->all(), $id), 'Referral updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->referral->destroy($id), 'Referral deleted successfully');
    }


}
