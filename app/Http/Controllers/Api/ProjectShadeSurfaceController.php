<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\CreateProjectShadeSurfaceRequest;
use Illuminate\Http\Request;
use App\Repositories\ProjectShadeSurface\ProjectShadeSurfaceRepository;

class ProjectShadeSurfaceController extends ApiBaseController
{
    private $projectShadeSurface;

    public function __construct(ProjectShadeSurfaceRepository $projectShadeSurface)
    {
        $this->projectShadeSurface = $projectShadeSurface;
    }

    public function index($projectId)
    {
        return $this->sendResponse($this->projectShadeSurface->all($projectId), 'ProjectShadeSurfaces fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->projectShadeSurface->find($id), 'Single ProjectShadeSurface retrieved successfully');
    }

    public function store(CreateProjectShadeSurfaceRequest $request)
    {
        return $this->sendResponse($this->projectShadeSurface->store($request->all()),'ProjectShadeSurface saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->projectShadeSurface->update($request->all(), $id), 'ProjectShadeSurface updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->projectShadeSurface->destroy($id), 'ProjectShadeSurface deleted successfully');
    }
}
