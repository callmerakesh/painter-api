<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('code')->nullable();
            $table->string('owner_name')->nullable();
            $table->string('address')->nullable();
            $table->string('status')->nullable();
            $table->string('price')->nullable();
            $table->date('start_date')->nullable();
            $table->date('completed_date')->nullable();
            $table->text('work_process')->nullable();
            $table->text('work_project_status')->nullable();
            $table->date('sample_start_date')->nullable();
            $table->boolean('is_active')->default(1);
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->unsignedBigInteger('referral_id')->nullable();
            $table->unsignedBigInteger('employee_id')->nullable();
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('referral_id')->references('id')->on('referrals');
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
