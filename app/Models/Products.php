<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class   Products extends Model
{
    use HasFactory;

    protected $table = 'products';

    protected $guarded = ['id'];

    public function category()
    {
        return $this->belongsTo(ProductTypes::class, 'product_type_id');
    }

    public function units()
    {
        return $this->belongsTo(Unit::class, 'unit_id');
    }

    public function getCreatedAtAttribute($data)
    {
        return Carbon::parse($data)->format('Y-m-d');
    }

    public function project()
    {
        return $this->belongsToMany(Project::class);
    }

    public function process()
    {
        return $this->belongsToMany(Process::class, 'product_process', 'product_id', 'process_id');
    }
}
