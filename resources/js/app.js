require('./bootstrap');

window.Vue = require('vue').default;

import Vuetify  from "vuetify";
import App from "./layout/App.vue";
import store from "./store/index";
import router from "./router";
import wysiwyg from "vue-wysiwyg";
import wysiwygCss from "vue-wysiwyg/dist/vueWysiwyg.css";
import { ValidationProvider } from 'vee-validate/dist/vee-validate.full.esm';
import { ValidationObserver} from "vee-validate";

Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);
Vue.use(wysiwygCss);
var VueEventBus = require('vue-event-bus');

Vue.use(Vuetify);
import 'vuetify/dist/vuetify.min.css';
import '@mdi/font/css/materialdesignicons.min.css';
Vue.use(VueEventBus);
window.user = JSON.parse(localStorage.getItem('user'));


Vue.use(wysiwyg, {
    hideModules: {
        "image": 'hide',
        'table': 'hide',
        'hyperlink': 'hide'
    },
});

new Vue({
    router,
    store,
    vuetify : new Vuetify(),
    render: h => h(App),
}).$mount('#app');
