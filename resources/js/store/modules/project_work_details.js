import $api from '../../handler/axios';
export const SET_PROJECT_DETAILS = 'SET_PROJECT_DETAILS';

const state = {
    projectDetails: {}
};

const mutations = {
    [SET_PROJECT_DETAILS](status, payload) {
        state.projectDetails = payload
    },

};

const actions = {
    async getSingleProjectDetail(context, id) {
        let url = `/project-detail/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllProjectDetails(context,payload) {
        let url =`/project-details/${payload.project_id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async addProjectDetail(context, formData) {
        let url = `/project-detail`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteProjectDetail(context, id) {
        let url = `/project-detail/${id}`;
        return await $api.delete(url);
    }

};

const getters  = {
    projectDetails : state => state.projectDetails,
};

export default {
    state,
    mutations,
    actions,
    getters
}
