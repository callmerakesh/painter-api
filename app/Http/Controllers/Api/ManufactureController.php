<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Manufacture\ManufactureRepository;
use App\Http\Requests\CreateManufactureRequest;

class ManufactureController extends ApiBaseController
{
    protected $manufacture;

    public function __construct(ManufactureRepository $manufacture)
    {
        $this->manufacture = $manufacture;
    }

    public function index(Request $request)
    {
        return $this->sendResponse($this->manufacture->all($request->all()), 'Manufactures fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->manufacture->find($id), 'Single Manufacture retrieved successfully');
    }

    public function store(CreateManufactureRequest $request)
    {
        return $this->sendResponse($this->manufacture->store($request->all()),'Manufacture saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->manufacture->update($request->all(), $id), 'Manufacture updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->manufacture->destroy($id), 'Manufacture deleted successfully');
    }
}
