<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    use HasFactory;

    protected $table = 'employees';

    protected $guarded = ['id'];

    protected $casts = ['is_active' => 'boolean'];

    public function profileImage()
    {
        return $this->hasMany(EmployeeImage::class, 'employee_id');
    }
    public function saveImage($document)
    {
        $filePath = public_path() . '/profileImages';
        if (!is_dir($filePath)) {
            mkdir($filePath, 0777, true);
        }
        $filename = Carbon::now()->timestamp. '_' . $document->getClientOriginalName();
        if ($document->move($filePath, $filename)) {
            return $filename;
        }
        return null;
    }

    public function resignDate()
    {
        return $this->hasMany(EmployeeResignDate::class, 'employee_id');
    }
}
