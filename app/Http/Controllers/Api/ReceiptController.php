<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Receipt\ReceiptRepository;

class ReceiptController extends ApiBaseController
{
    public $receipt;

    public function __construct(ReceiptRepository $receipt)
    {
        $this->receipt = $receipt;
    }

    public function index($projectId)
    {
        return $this->sendResponse($this->receipt->all($projectId), 'Receipts fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->receipt->find($id), 'Single Receipt retrieved successfully');
    }

    public function store(Request $request)
    {
        return $this->sendResponse($this->receipt->store($request->all()),'Receipt saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->receipt->update($request->all(), $id), 'Receipt updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->receipt->destroy($id), 'Receipt deleted successfully');
    }

    public function transferMaterialFromProject(Request $request)
    {
        return $this->sendResponse($this->receipt->transferMaterialFromProject($request->all()), 'Material Transferred Successfully');
    }
}
