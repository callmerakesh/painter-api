export const SET_VALIDATION = 'SET_VALIDATION';
export const CLEAR_VALIDATIONS = 'CLEAR_VALIDATIONS';

const state = {
    validation: {}
};

const mutations = {
    [SET_VALIDATION](state, payload) {
        state.validation = payload;
    },
    [CLEAR_VALIDATIONS] (state) {
        state.validation = {}
    }
};

const actions = {
    setValidation:(context, error) => {
        context.commit(CLEAR_VALIDATIONS);
        const validationErrors = error.response.data.errors;
        let validation = {};
        Object.entries(validationErrors).forEach(([key, value]) => {
            validation = {
                ...validation,
                [key] : value[0]
            }
        });
        context.commit(SET_VALIDATION, validation);
    }
};

const getters = {
    validation : state => state.validation,
};

export default {
    state,
    mutations,
    actions,
    getters
}
