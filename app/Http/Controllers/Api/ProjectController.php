<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\CreateProjectRequest;
use Illuminate\Http\Request;
use App\Repositories\Project\ProjectRepository;


class ProjectController extends ApiBaseController
{
    public $project;

    public function __construct(ProjectRepository $project)
    {
        $this->project = $project;
    }

    public function index(Request $request)
    {
        return $this->sendResponse($this->project->all($request->all()), 'Projects fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->project->find($id), 'Single Project retrieved successfully');
    }

    public function store(CreateProjectRequest $request)
    {
        return $this->sendResponse($this->project->store($request->all()),'Project saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->project->update($request->all(), $id), 'Project updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->project->destroy($id), 'Project deleted successfully');
    }

    public function toggleProject(Request $request, $id)
    {
        return $this->sendResponse($this->project->toggleProject($id), 'Project toggled Successfully');
    }

    public function getTransferProjects(Request $request, $id)
    {
        return $this->sendResponse($this->project->getTransferProjects($id), 'Transfer Projects Retrieved Successfully');
    }

}
