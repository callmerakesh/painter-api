<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
})->name('login')->middleware('guest');

Route::post('/login', '\App\Http\Controllers\LoginController@login')->name('user.login')->middleware('guest');
Route::get('/logout', '\App\Http\Controllers\LoginController@logout')->name('user.logout')->middleware('auth');

Route::get('/dashboard', '\App\Http\Controllers\DashboardController@index')->middleware('auth');

Route::get('/{any}', '\App\Http\Controllers\DashboardController@index')->where('any', '.*')->middleware('auth');
