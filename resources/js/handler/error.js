let errorHandler = {

    errorCodeHandler: function(errorCode, error, $this) {
        switch (errorCode) {
            case 400:
                break;

            case 401:
                break;

            case 404:
                //Resource Not Found;
                break;

            case 403:
                //Authentication Failed!;
                break;

            case 422:
                break;

            case 500:
                break;

            default:
                break;
        }
    },
};

export default errorHandler;
