<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\CreateContractRequest;
use App\Repositories\Contract\ContractRepository;
use Illuminate\Http\Request;

class ContractController extends ApiBaseController
{
    private $contract;

    public function __construct(ContractRepository $contract)
    {
        $this->contract = $contract;
    }

    public function index(Request $request)
    {
        return $this->sendResponse($this->contract->all($request->all()), 'Contracts fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->contract->find($id), 'Single Contract retrieved successfully');
    }

    public function store(CreateContractRequest $request)
    {
        return $this->sendResponse($this->contract->store($request->all()),'Contract saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->contract->update($request->all(), $id), 'Contract updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->contract->destroy($id), 'Contract deleted successfully');
    }
}
