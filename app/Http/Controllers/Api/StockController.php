<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\StockRequest;
use Illuminate\Http\Request;
use App\Repositories\Stock\StockRepository;

class StockController extends ApiBaseController
{
    private $stock;

    public function __construct(StockRepository $stock)
    {
        $this->stock = $stock;
    }

    public function index(Request $request)
    {
        return $this->sendResponse($this->stock->all($request->all()), 'Equipment Categories fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->stock->find($id), 'Single Equipment Category retrieved successfully');
    }

    public function store(StockRequest $request)
    {
        return $this->sendResponse($this->stock->store($request->all()),'Equipment Category saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->stock->update($request->all(), $id), 'Equipment Category updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->stock->destroy($id), 'Equipment Category deleted successfully');
    }
}
