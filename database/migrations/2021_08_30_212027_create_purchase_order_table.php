<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('project_id')->nullable();
            $table->unsignedBigInteger('requisition_id')->nullable();
            $table->unsignedBigInteger('material_id')->nullable();
            $table->unsignedBigInteger('pack_id')->nullable();
            $table->unsignedBigInteger('sn')->index();
            $table->date('date')->nullable();
            $table->unsignedBigInteger('vendor_id')->nullable();
            $table->string('pack_unit')->nullable();
            $table->string('quantity')->nullable();
            $table->string('details')->nullable();
            $table->string('description')->nullable();
            $table->softDeletes();
            $table->foreign('project_id')->references('id')->on('products')->onDelete('set null');
            $table->foreign('requisition_id')->references('id')->on('requisition')->onDelete('set null');
            $table->foreign('vendor_id')->references('id')->on('vendors')->onDelete('set null');
            $table->foreign('material_id')->references('id')->on('materials')->onDelete('set null');
            $table->foreign('pack_id')->references('id')->on('pack_types')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
