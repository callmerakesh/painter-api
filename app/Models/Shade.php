<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shade extends Model
{
    use HasFactory;

    protected $table = 'shade_cards';

    protected $guarded = ['id'];


    public function manufacture()
    {
        return $this->belongsTo(Manufacture::class, 'manufacture_id');
    }
}
