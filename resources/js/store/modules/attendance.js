import $api from '../../handler/axios';
export const SET_ATTENDANCE = 'SET_ATTENDANCE';

const state = {
    attendances: {}
};

const mutations = {
    [SET_ATTENDANCE] (status, payload) {
        state.attendances = payload
    }
};

const actions = {
    async getSingleAttendance(context, id) {
        let url = `/attendance/${id}`;
        let data = await $api.get(url);
        return  data.data;
    },

    async getAllAttendance(context, workDetailId) {
        let url = `all-attendance/${workDetailId}`;
        let data = await $api.get(url);
        return data.data;
    },

    async addAttendance(context, formData) {
        let url = `/attendance`;
        let data = await $api.post(url, formData);
        return data.data;
    },

    async addPhaseAttendance(context, formData) {
        let url = `/first-phase-attendance`;
        let data = await $api.post(url, formData);
        return data.data;
    },

    async deleteAttendance(context, id) {
        let url = `/attendance/${id}`;
        return await $api.delete(url);
    },

    async getTodayAttendance(context) {
        let url = `/today-attendance`;
        let data = await $api.get(url);
        return data.data;
    },

    async getTodayAbsentAttendance(context) {
        let url = `/today-absent-attendance`;
        let data = await $api.get(url);
        return data.data;
    },

    async addPhaseAttendanceJobEnd(context, formData) {
        let url = `/phase-attendance-job-end`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async editPhaseAttendance(context, formData) {
        let url = `/edit-first-phase-attendance`;
        let data = await $api.post(url, formData);
        return data.data;
    }
};

const getters =  {
    attendances : state => state.atteqndances,
};

export default {
    state,
    mutations,
    actions,
    getters
}
