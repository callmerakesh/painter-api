<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        \App\Models\User::create([
            'name' => 'Rakesh Ghimire',
            'email' => 'ghimirerakesh1996@gmail.com',
             'password' => bcrypt('123456')
        ]);
        \App\Models\User::create([
            'name' => 'Varun Sir',
            'email' => 'varun@ultimateexin.com',
            'password' => bcrypt('varun@321')
        ]);
    }
}
