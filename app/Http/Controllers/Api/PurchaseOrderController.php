<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\PurchaseOrder\PurchaseOrderRepository;

class PurchaseOrderController extends ApiBaseController
{
    public $purchaseOrder;

    public function __construct(PurchaseOrderRepository $purchaseOrder)
    {
        $this->purchaseOrder = $purchaseOrder;
    }

    public function index($projectId)
    {
        return $this->sendResponse($this->purchaseOrder->all($projectId), 'Purchase Orders fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->purchaseOrder->find($id), 'Single Purchase Order retrieved successfully');
    }

    public function store(Request $request)
    {
        return $this->sendResponse($this->purchaseOrder->store($request->all()),'Purchase Order saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->purchaseOrder->update($request->all(), $id), 'Purchase Order updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->purchaseOrder->destroy($id), 'Purchase Order deleted successfully');
    }
}
