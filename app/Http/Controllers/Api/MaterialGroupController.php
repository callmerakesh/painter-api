<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use Illuminate\Http\Request;
use App\Http\Requests\CreateMaterialGroupRequest;
use App\Repositories\MaterialGroup\MaterialGroupRepository;


class MaterialGroupController extends ApiBaseController
{
    private $materialGroup;

    public function __construct(MaterialGroupRepository $materialGroup)
    {
        $this->materialGroup = $materialGroup;
    }

    public function index(Request $request)
    {
        return $this->sendResponse($this->materialGroup->all($request->all()), 'Material Groups fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->materialGroup->find($id), 'Single Material Group retrieved successfully');
    }

    public function store(CreateMaterialGroupRequest $request)
    {
        return $this->sendResponse($this->materialGroup->store($request->all()),'Material Group saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->materialGroup->update($request->all(), $id), 'Material Group updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->materialGroup->destroy($id), 'Material Group deleted successfully');
    }
}
