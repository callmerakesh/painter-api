<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if(env('APP_ENV') == 'production') {
            $this->app->bind('path.public', function () {
                return base_path() . '/../public_html';
            });
        }
        $this->app->singleton(
            \App\Repositories\Receipt\ReceiptRepository::class,
            \App\Repositories\Receipt\ReceiptEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\PurchaseOrder\PurchaseOrderRepository::class,
            \App\Repositories\PurchaseOrder\PurchaseOrderEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\Requisition\RequisitionRepository::class,
            \App\Repositories\Requisition\RequisitionEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\RawMaterialQuotation\RawMaterialQuotationRepository::class,
            \App\Repositories\RawMaterialQuotation\RawMaterialQuotationEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\ShadePalette\ShadePaletteRepository::class,
            \App\Repositories\ShadePalette\ShadePaletteEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\MaterialGroup\MaterialGroupRepository::class,
            \App\Repositories\MaterialGroup\MaterialGroupEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\PackType\PackTypeRepository::class,
            \App\Repositories\PackType\PackTypeEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\ProjectShadeSurface\ProjectShadeSurfaceRepository::class,
            \App\Repositories\ProjectShadeSurface\ProjectShadeSurfaceEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\ProjectShadeRoom\ProjectShadeRoomRepository::class,
            \App\Repositories\ProjectShadeRoom\ProjectShadeRoomEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\ShadeCard\ShadeCardRepository::class,
            \App\Repositories\ShadeCard\ShadeCardEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\Manufacture\ManufactureRepository::class,
            \App\Repositories\Manufacture\ManufactureEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\Contract\ContractRepository::class,
            \App\Repositories\Contract\ContractEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\Process\ProcessRepository::class,
            \App\Repositories\Process\ProcessEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\Attendance\AttendanceRepository::class,
            \App\Repositories\Attendance\AttendanceEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\EquipmentCategory\EquipmentCategoryRepository::class,
            \App\Repositories\EquipmentCategory\EquipmentCategoryEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\Equipment\EquipmentRepository::class,
            \App\Repositories\Equipment\EquipmentEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\Stock\StockRepository::class,
            \App\Repositories\Stock\StockEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\Unit\UnitRepository::class,
            \App\Repositories\Unit\UnitEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\ProjectDetail\ProjectDetailRepository::class,
            \App\Repositories\ProjectDetail\ProjectDetailEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\Project\ProjectRepository::class,
            \App\Repositories\Project\ProjectEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\Employees\EmployeeRepository::class,
            \App\Repositories\Employees\EmployeeEloquent::class

        );
        $this->app->singleton(
            \App\Repositories\ProductTypes\ProductTypeRepository::class,
            \App\Repositories\ProductTypes\ProductTypeEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\Products\ProductRepository::class,
            \App\Repositories\Products\ProductEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\Referrals\ReferralRepository::class,
            \App\Repositories\Referrals\ReferralEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\Customers\CustomerRepository::class,
            \App\Repositories\Customers\CustomerEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\Vendors\VendorRepository::class,
            \App\Repositories\Vendors\VendorEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\Materials\MaterialRepository::class,
            \App\Repositories\Materials\MaterialEloquent::class
        );
        $this->app->singleton(
            \App\Repositories\User\UserRepository::class,
            \App\Repositories\User\UserEloquent::class
        );

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
