<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    use HasFactory;

    protected $table = 'customers';

    protected $guarded = ['id'];

   public function referral()
    {
        return $this->belongsTo(\App\Models\Referrals::class,'referral_id');
    }
    public function parent()
    {
        return $this->belongsTo(\App\Models\Customers::class,'parent_id');
    }


    public function getCreatedAtAttribute($data)
    {
        return Carbon::parse($data)->format('Y-m-d');
    }
}
