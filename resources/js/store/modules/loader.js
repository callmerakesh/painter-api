export const BUTTON_LOADER = "BUTTON_LOADER";

const state = {
    buttonLoader: false
};

const mutations = {
    [BUTTON_LOADER] (state, value) {
        state.buttonLoader = value
    }
};

const actions = {

};

const getters = {
    buttonLoader : state => state.buttonLoader
};

export default {
    state,
    mutations,
    actions,
    getters
}
