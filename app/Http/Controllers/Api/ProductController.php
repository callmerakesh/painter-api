<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\CreateProductRequest;
use App\Repositories\Products\ProductRepository;
use Illuminate\Http\Request;

class ProductController extends ApiBaseController
{
    private $product;

    public function __construct(ProductRepository $product)
    {
        $this->product = $product;
    }

    public function index(Request $request)
    {
        return $this->sendResponse($this->product->all($request->all()), 'Products fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->product->find($id), 'Single Product retrieved successfully');
    }

    public function store(CreateProductRequest $request)
    {
        return $this->sendResponse($this->product->store($request->all()),'Product saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->product->update($request->all(), $id), 'Product updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->product->destroy($id), 'Product deleted successfully');
    }

    public function getCategoryProduct($categoryId)
    {
        return $this->sendResponse($this->product->getCategoryProduct($categoryId), 'Category Product Retrieved Successfully');
    }

    public function syncProcess(Request $request)
    {
        return $this->sendResponse($this->product->syncProcess($request->all()), 'Product Process Synced Successfully');
    }
}
