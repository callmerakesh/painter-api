import $api from '../../handler/axios';
export const SET_EQUIPMENT = 'SET_EQUIPMENT';

const state = {
    equipments: {}
};

const mutations = {
    [SET_EQUIPMENT](status, payload) {
        state.equipments = payload
    },

};

const actions = {
    async getSingleEquipment(context, id) {
        let url = `/equipment/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllEquipments(context) {
        let url =`/equipments`;
        let data = await $api.get(url);
        return data.data;
    },
    async addEquipment(context, formData) {
        let url = `/equipment`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteEquipment(context, id) {
        let url = `/equipment/${id}`;
        return await $api.delete(url);
    }
};

const getters  = {
    equipments : state => state.equipments,
};

export default {
    state,
    mutations,
    actions,
    getters
}
