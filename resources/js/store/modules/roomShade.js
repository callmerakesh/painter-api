import $api from '../../handler/axios';
export const SET_ROOM_SHADES = 'SET_ROOM_SHADES';

const state = {
    roomShades: {}
};

const mutations = {
    [SET_ROOM_SHADES](status, payload) {
        state.roomShades = payload
    },

};

const actions = {
    async getSingleProjectRoomShade(context, id) {
        let url = `/project-shade-room/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllProjectRoomShades(context,payload) {
        let url =`/project-shade-rooms/${payload.project_id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async addProjectRoomShade(context, formData) {
        let url = `/project-shade-room`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteProjectRoomShade(context, id) {
        let url = `/project-shade-room/${id}`;
        return await $api.delete(url);
    }

};

const getters  = {
    roomShades : state => state.roomShades,
};

export default {
    state,
    mutations,
    actions,
    getters
}
