<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectShadeRoom extends Model
{
    use HasFactory;

    protected $table = 'project_shade_rooms';

    protected $guarded = ['id'];

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function shadeCard()
    {
        return $this->belongsTo(Shade::class, 'shade_card_id');
    }

    public function surface()
    {
        return $this->hasMany(ProjectShadeSurface::class, 'room_id');
    }
}
