<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Process extends Model
{
    use HasFactory;

    protected $table = 'process';

    protected $guarded = ['id'];

    public function getCreatedAtAttribute($data)
    {
        return Carbon::parse($data)->format('Y-m-d');
    }
}
