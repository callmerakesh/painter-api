<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ShadeCard\ShadeCardRepository;
use App\Http\Requests\ShadeCardRequest;

class ShadeCardController extends ApiBaseController
{
    private $shade;

    public function __construct(ShadeCardRepository $shade)
    {
        $this->shade = $shade;
    }

    public function index(Request $request)
    {
        return $this->sendResponse($this->shade->all($request->all()), 'Shade Cards fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->shade->find($id), 'Single Shade Card retrieved successfully');
    }

    public function store(ShadeCardRequest $request)
    {
        return $this->sendResponse($this->shade->store($request->all()),'Shade Card saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->shade->update($request->all(), $id), 'Shade Card updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->shade->destroy($id), 'Shade Card deleted successfully');
    }
}
