import $api from '../../handler/axios';
export const SET_SHADE_PALETTES = 'SET_SHADE_PALETTES';

const state = {
    shadePalettes: {}
};

const mutations = {
    [SET_SHADE_PALETTES](status, payload) {
        state.shadePalettes = payload
    },

};

const actions = {
    async getSingleShadePalette(context, id) {
        let url = `/shade-palette/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllShadePalettes(context) {
        let url =`/shade-palettes`;
        let data = await $api.get(url);
        return data.data;
    },
    async addShadePalette(context, formData) {
        let url = `/shade-palette`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteShadePalette(context, id) {
        let url = `/shade-palette/${id}`;
        return await $api.delete(url);
    }
};

const getters  = {
    shadePalettes : state => state.shadePalettes,
};

export default {
    state,
    mutations,
    actions,
    getters
}
