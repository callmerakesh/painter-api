<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\CreateProductTypeRequest;
use App\Repositories\ProductTypes\ProductTypeRepository;
use Illuminate\Http\Request;

class ProductTypeController extends ApiBaseController
{
    private $product_type;

    public function __construct(ProductTypeRepository $product_type)
    {
        $this->product_type = $product_type;
    }

    public function index(Request $request)
    {
        return $this->sendResponse($this->product_type->all($request->all()), 'ProductTypes fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->product_type->find($id), 'Single ProductType retrieved successfully');
    }

    public function store(CreateProductTypeRequest $request)
    {
        return $this->sendResponse($this->product_type->store($request->all()),'ProductType saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->product_type->update($request->all(), $id), 'ProductType updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->product_type->destroy($id), 'ProductType deleted successfully');
    }


}
