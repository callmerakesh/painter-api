<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\CreateUnitRequest;
use App\Repositories\Unit\UnitRepository;
use Illuminate\Http\Request;

class UnitController extends ApiBaseController
{
    private $unit;

    public function __construct(UnitRepository $unit)
    {
        $this->unit = $unit;
    }

    public function index(Request $request)
    {
        return $this->sendResponse($this->unit->all($request->all()), 'Units fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->unit->find($id), 'Single Unit retrieved successfully');
    }

    public function store(CreateUnitRequest $request)
    {
        return $this->sendResponse($this->unit->store($request->all()),'Unit saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->unit->update($request->all(), $id), 'Unit updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->unit->destroy($id), 'Unit deleted successfully');
    }
}
