import $api from '../../handler/axios';
export const SET_PROCESSES = 'SET_PROCESSES';

const state = {
    processes: {}
};

const mutations = {
    [SET_PROCESSES](status, payload) {
        state.processes = payload
    },

};

const actions = {
    async getSingleProcess(context, id) {
        let url = `/process/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllProcesses(context) {
        let url =`/processes`;
        let data = await $api.get(url);
        return data.data;
    },
    async addProcess(context, formData) {
        let url = `/process`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteProcess(context, id) {
        let url = `/process/${id}`;
        return await $api.delete(url);
    }
};

const getters  = {
    processes : state => state.processes,
};

export default {
    state,
    mutations,
    actions,
    getters
}
