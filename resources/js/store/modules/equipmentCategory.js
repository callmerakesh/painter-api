import $api from '../../handler/axios';
export const SET_EQUIPMENT_CATEGORY = 'SET_EQUIPMENT_CATEGORY';

const state = {
    equipmentCategories: {}
};

const mutations = {
    [SET_EQUIPMENT_CATEGORY](status, payload) {
        state.equipmentCategories = payload
    },
};

const actions = {
    async getSingleEquipmentCategory(context, id) {
        let url = `/equipment-category/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllEquipmentCategories(context) {
        let url =`/equipment-categories`;
        let data = await $api.get(url);
        return data.data;
    },
    async addEquipmentCategory(context, formData) {
        let url = `/equipment-category`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteEquipmentCategory(context, id) {
        let url = `/equipment-category/${id}`;
        return await $api.delete(url);
    }
};

const getters  = {
    equipmentCategories : state => state.equipmentCategories,
};

export default {
    state,
    mutations,
    actions,
    getters
}
