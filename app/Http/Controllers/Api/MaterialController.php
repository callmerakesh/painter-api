<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\CreateMaterialRequest;
use App\Repositories\Materials\MaterialRepository;
use Illuminate\Http\Request;

class MaterialController extends ApiBaseController
{
    private $product;

    public function __construct(MaterialRepository $product)
    {
        $this->product = $product;
    }

    public function index(Request $request)
    {
        return $this->sendResponse($this->product->all($request->all()), 'Materials fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->product->find($id), 'Single Material retrieved successfully');
    }

    public function store(Request $request)
    {
        return $this->sendResponse($this->product->store($request->all()),'Material saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->product->update($request->all(), $id), 'Material updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->product->destroy($id), 'Material deleted successfully');
    }

    public function getTransferMaterials(Request $request, $id)
    {
        return $this->sendResponse($this->product->getTransferMaterials($id), 'Transfer Materials Retrieved Successfully');
    }


}
