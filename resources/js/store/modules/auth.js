import $api from '../../handler/axios'

const state = {};

const mutations = {};

const actions = {
    async logout(){
        let url = `/logout`;
        await $api.get(url);
    }
};

const getters = {};

export default {
    state,
    mutations,
    actions,
    getters
}
