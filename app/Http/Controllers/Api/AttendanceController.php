<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use Illuminate\Http\Request;
use App\Repositories\Attendance\AttendanceRepository;
use App\Http\Requests\CreateAttendanceRequest;

class AttendanceController extends ApiBaseController
{

    private $attendance;

    public function __construct(AttendanceRepository $attendance)
    {
        $this->attendance = $attendance;
    }

    public function index($id)
    {
        return $this->sendResponse($this->attendance->all($id), 'All Attendance fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->attendance->find($id), 'Single Attendance retrieved successfully');
    }

    public function store(CreateAttendanceRequest $request)
    {
        return $this->sendResponse($this->attendance->store($request->all()),'Attendance saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->attendance->update($request->all(), $id), 'Attendance updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->attendance->destroy($id), 'Attendance deleted successfully');
    }

    public function getTodayAttendance(Request $request)
    {
        return $this->sendResponse($this->attendance->getTodayAttendance($request->all()),'Today Attendance Retrieved Successfully');
    }

    public function getTodayAbsentAttendance(Request $request)
    {
        return $this->sendResponse($this->attendance->getTodayAbsentAttendance($request->all()), 'Today Absent Attendance Retrieved Successfully');
    }

    public function storeFirstPhaseAttendance(Request $request)
    {
        return $this->sendResponse($this->attendance->storeFirstPhaseAttendance($request->all()), 'First Phase Attendance Stored Successfully');
    }

    public function editFirstPhaseAttendance(Request $request)
    {
        return $this->sendResponse($this->attendance->editFirstPhaseAttendance($request->all()), 'First Phase Attendance Edited Successfully');

    }

    public function addPhaseAttendanceJobEnd(Request $request)
    {
        return $this->sendResponse($this->attendance->addPhaseAttendanceJobEnd($request->all()), 'First Phase Attendance Job End Date Added Successfully');
    }
}
