import $api from '../../handler/axios';
export const SET_REQUISITION = 'SET_REQUISITION';

const state = {
    requisitions: {}
};

const mutations = {
    [SET_REQUISITION](status, payload) {
        state.requisitions = payload
    },

};

const actions = {
    async getSingleRequisition(context, id) {
        let url = `/requisition/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllRequisitions(context, payload) {
        let url =`/requisitions/${payload.project_id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async addRequisition(context, formData) {
        let url = `/requisition`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteRequisition(context, id) {
        let url = `/requisition/${id}`;
        return await $api.delete(url);
    }
};

const getters  = {
    requisitions : state => state.requisitions,
};

export default {
    state,
    mutations,
    actions,
    getters
}
