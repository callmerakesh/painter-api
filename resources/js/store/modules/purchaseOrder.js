import $api from '../../handler/axios';
export const SET_PURCHASE_ORDER = 'SET_PURCHASE_ORDER';

const state = {
    purchaseOrders: {}
};

const mutations = {
    [SET_PURCHASE_ORDER](status, payload) {
        state.purchaseOrders = payload
    },

};

const actions = {
    async getSinglePurchaseOrder(context, id) {
        let url = `/purchase-order/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllPurchaseOrders(context, payload) {
        let url =`/purchase-orders/${payload.project_id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async addPurchaseOrder(context, formData) {
        let url = `/purchase-order`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deletePurchaseOrder(context, id) {
        let url = `/purchase-order/${id}`;
        return await $api.delete(url);
    }
};

const getters  = {
    purchaseOrders : state => state.purchaseOrders,
};

export default {
    state,
    mutations,
    actions,
    getters
}
