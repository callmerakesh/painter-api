<!DOCTYPE html>
<html>
<head>
    <link rel="icon" type="image/jpg" href="{{asset("images/Exin.jpg")}}">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font/css/materialdesignicons.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>Ultimate EX-IN painting solution</title>
</head>
<body>
<div id="app">
    <v-app>
        <v-content>
            <v-container></v-container>
        </v-content>
    </v-app>
</div>
<script src="{{asset('/js/manifest.js') }}" defer></script>
<script src="{{asset('/js/vendor.js') }}" defer></script>
<script src="{{ asset('js/app.js') }}" defer></script>
<script>
    {{--var user = '{!!  addslashes(json_encode(getLoggedInUserData()))!!}';--}}
    {{--window.localStorage.setItem('user', user);--}}
</script>

</body>
</html>
