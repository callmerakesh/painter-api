<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShadePalette extends Model
{
    use HasFactory;

    protected $table = 'shade_palette';

    protected $guarded = ['id'];


    public function shadeCard()
    {
        return $this->belongsTo(Shade::class, 'shade_id');
    }
}
