import $api from '../../handler/axios';
export const SET_UNITS = 'SET_UNITS';

const state = {
    units: {}
};

const mutations = {
    [SET_UNITS](status, payload) {
        state.units = payload
    },

};

const actions = {
    async getSingleUnit(context, id) {
        let url = `/unit/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllUnits(context) {
        let url =`/units`;
        let data = await $api.get(url);
        return data.data;
    },
    async addUnit(context, formData) {
        let url = `/unit`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteUnit(context, id) {
        let url = `/unit/${id}`;
        return await $api.delete(url);
    }
};

const getters  = {
    units : state => state.units,
};

export default {
    state,
    mutations,
    actions,
    getters
}
