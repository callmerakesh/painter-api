import $api from '../../handler/axios';
export const SET_STOCKS = 'SET_STOCKS';

const state = {
    stocks: {}
};

const mutations = {
    [SET_STOCKS](status, payload) {
        state.stocks = payload
    },

};

const actions = {
    async getSingleStock(context, id) {
        let url = `/stock/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllStocks(context) {
        let url =`/stocks`;
        let data = await $api.get(url);
        return data.data;
    },
    async addStock(context, formData) {
        let url = `/stock`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteStock(context, id) {
        let url = `/stock/${id}`;
        return await $api.delete(url);
    }
};

const getters  = {
    stocks : state => state.stocks,
};

export default {
    state,
    mutations,
    actions,
    getters
}
