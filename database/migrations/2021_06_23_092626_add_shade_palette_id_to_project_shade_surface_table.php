<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShadePaletteIdToProjectShadeSurfaceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_shade_surfaces', function (Blueprint $table) {
            $table->unsignedBigInteger('shade_palette_id')->nullable();
            $table->foreign('shade_palette_id')->references('id')->on('shade_palette')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_shade_surfaces', function (Blueprint $table) {
            //
        });
    }
}
