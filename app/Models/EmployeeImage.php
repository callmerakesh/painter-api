<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeImage extends Model
{
    use HasFactory;

    protected $table = 'employee_image';

    protected $guarded = ['id'];

    public function employee()
    {
        return $this->belongsTo(Employees::class);
    }

    public function getPathAttribute($path)
    {
        return url('/') . '/profileImages/' . $path;
    }

    public function getActualPathAttribute()
    {
        return $this->path;
    }

}
