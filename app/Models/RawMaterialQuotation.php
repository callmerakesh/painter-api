<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RawMaterialQuotation extends Model
{
    use HasFactory;

    protected $table = 'raw_material_quotation';

    protected $guarded = ['id'];


    public function vendor()
    {
        return $this->belongsTo(Vendors::class, 'vendor_id');
    }

    public function material()
    {
        return $this->belongsTo(Materials::class, 'material_id');
    }

    public function pack()
    {
        return $this->belongsTo(PackType::class, 'pack_id');
    }
}
