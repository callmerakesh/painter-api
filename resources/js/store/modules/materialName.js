import $api from '../../handler/axios';
export const SET_MATERIAL_NAMES = 'SET_MATERIAL_NAMES';

const state = {
    materialNames: {}
};

const mutations = {
    [SET_MATERIAL_NAMES](status, payload) {
        state.materialNames = payload
    },

};

const actions = {
    async getSingleMaterialName(context, id) {
        let url = `/material-name/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllMaterialNames(context) {
        let url =`/material-names`;
        let data = await $api.get(url);
        return data.data;
    },
    async addMaterialName(context, formData) {
        let url = `/material-name`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteMaterialName(context, id) {
        let url = `/material-name/${id}`;
        return await $api.delete(url);
    }
};

const getters  = {
    materialNames : state => state.materialNames,
};

export default {
    state,
    mutations,
    actions,
    getters
}
