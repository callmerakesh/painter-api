import $api from '../../handler/axios';
export const SET_MANUFACTURES = 'SET_MANUFACTURES';

const state = {
    manufactures: {}
};

const mutations = {
    [SET_MANUFACTURES](status, payload) {
        state.manufactures = payload
    },

};

const actions = {
    async getSingleManufacture(context, id) {
        let url = `/manufacture/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllManufactures(context) {
        let url =`/manufactures`;
        let data = await $api.get(url);
        return data.data;
    },
    async addManufacture(context, formData) {
        let url = `/manufacture`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteManufacture(context, id) {
        let url = `/manufacture/${id}`;
        return await $api.delete(url);
    }
};

const getters  = {
    manufactures : state => state.manufactures,
};

export default {
    state,
    mutations,
    actions,
    getters
}
