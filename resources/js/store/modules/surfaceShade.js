import $api from '../../handler/axios';
export const SET_SURFACE_SHADES = 'SET_SURFACE_SHADES';

const state = {
    surfaceShades: {}
};

const mutations = {
    [SET_SURFACE_SHADES](status, payload) {
        state.surfaceShades = payload
    },

};

const actions = {
    async getSingleProjectSurfaceShade(context, id) {
        let url = `/project-shade-surface/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllProjectSurfaceShades(context,payload) {
        let url =`/project-shade-surfaces/${payload.project_id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async addProjectSurfaceShade(context, formData) {
        let url = `/project-shade-surface`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteProjectSurfaceShade(context, id) {
        let url = `/project-shade-surface/${id}`;
        return await $api.delete(url);
    }

};

const getters  = {
    surfaceShades : state => state.surfaceShades,
};

export default {
    state,
    mutations,
    actions,
    getters
}
