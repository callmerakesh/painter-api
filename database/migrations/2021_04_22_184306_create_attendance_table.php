<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance', function (Blueprint $table) {
            $table->id();
            $table->date('date')->nullable();
            $table->unsignedBigInteger('employee_id')->nullable();
            $table->time('job_start')->nullable();
            $table->time('job_end')->nullable();
            $table->unsignedBigInteger('project_id')->nullable();
            $table->unsignedBigInteger('project_work_detail_id')->nullable();
            $table->unsignedBigInteger('process_id')->nullable();
            $table->unsignedBigInteger('stock_id')->nullable();
            $table->string('item_description')->nullable();
            $table->double('quantity')->nullable();
            $table->unsignedBigInteger('equipment_id')->nullable();
            $table->time('process_start_time')->nullable();
            $table->time('process_end_time')->nullable();
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('project_work_detail_id')->references('id')->on('project_work_details');
            $table->foreign('process_id')->references('id')->on('process');
            $table->foreign('stock_id')->references('id')->on('stocks');
            $table->foreign('equipment_id')->references('id')->on('equipment_table');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance');
    }
}
