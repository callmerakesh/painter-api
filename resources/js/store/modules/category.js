import $api from '../../handler/axios';
export const SET_CATEGORIES = 'SET_CATEGORIES';

const state = {
    categories: {}
};

const mutations = {
    [SET_CATEGORIES](status, payload) {
        state.categories = payload
    },

};

const actions = {
    async getSingleCategory(context, id) {
        let url = `/category/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllCategories(context,payload) {
        let url =`/categories?page=${payload.page}&search=${payload.search}&type=${payload.type}`;
        let data = await $api.get(url);
        return data.data;
    },
    async addCategory(context, formData) {
        let url = `/category`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteCategory(context, id) {
        let url = `/category/${id}`;
        return await $api.delete(url);
    },

};

const getters  = {
    categories : state => state.categories,
};

export default {
    state,
    mutations,
    actions,
    getters
}
