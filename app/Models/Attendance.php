<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    use HasFactory;

    protected $table = 'attendance';

    protected $guarded = ['id'];

    public function employee()
    {
        return $this->belongsTo(Employees::class, 'employee_id');
    }

    public function getCreatedAtAttribute($data)
    {
        return Carbon::parse($data)->format('Y-m-d');
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function projectDetail()
    {
        return $this->belongsTo(ProjectDetail::class,'project_work_detail_id');
    }

    public function process()
    {
        return $this->belongsTo(Process::class, 'process_id');
    }

    public function stock()
    {
        return $this->belongsTo(Stocks::class, 'stock_id');
    }

    public function equipment()
    {
        return $this->belongsTo(Equipment::class, 'equipment_id');
    }
}
