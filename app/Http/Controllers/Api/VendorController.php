<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\CreateVendorRequest;
use App\Repositories\Vendors\VendorRepository;
use Illuminate\Http\Request;

class VendorController extends ApiBaseController
{
    private $vendor;

    public function __construct(VendorRepository $vendor)
    {
        $this->vendor = $vendor;
    }

    public function index(Request $request)

    {
        return $this->sendResponse($this->vendor->all($request->all()), 'Vendors fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->vendor->find($id), 'Single Vendor retrieved successfully');
    }

    public function store(CreateVendorRequest $request)
    {
        return $this->sendResponse($this->vendor->store($request->all()),'Vendor saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->vendor->update($request->all(), $id), 'Vendor updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->vendor->destroy($id), 'Vendor deleted successfully');
    }


}
