<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectShadeSurface extends Model
{
    use HasFactory;

    protected $table = 'project_shade_surfaces';

    protected $guarded = ['id'];

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function shadeCard()
    {
        return $this->belongsTo(Shade::class, 'shade_card_id');
    }

    public function material()
    {
        return $this->belongsTo(Materials::class, 'material_id');
    }

    public function room()
    {
        return $this->belongsTo(ProjectShadeRoom::class, 'room_id');
    }
}
