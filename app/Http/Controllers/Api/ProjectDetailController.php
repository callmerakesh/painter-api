<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\CreateProjectDetailRequest;
use Illuminate\Http\Request;
use App\Repositories\ProjectDetail\ProjectDetailRepository;

class ProjectDetailController extends ApiBaseController
{
    public $detail;

    public function __construct(ProjectDetailRepository $detail)
    {
        $this->detail = $detail;
    }
    public function index($projectId)
    {
        return $this->sendResponse($this->detail->all($projectId), 'Projects fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->detail->find($id), 'Single Project retrieved successfully');
    }

    public function store(CreateProjectDetailRequest $request)
    {
        return $this->sendResponse($this->detail->store($request->all()),'Project saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->detail->update($request->all(), $id), 'Project updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->detail->destroy($id), 'Project deleted successfully');
    }
}
