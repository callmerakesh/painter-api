<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectShadeSurfaceDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_shade_surfaces', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('project_id')->nullable();
            $table->unsignedBigInteger('shade_card_id')->nullable();
            $table->unsignedBigInteger('material_id')->nullable();
            $table->unsignedBigInteger('room_id')->nullable();
            $table->text('surface_description')->nullable();
            $table->string('shade_name')->nullable();
            $table->string('shade_id')->nullable();
            $table->string('chosen_by')->nullable();
            $table->date('chosen_date')->nullable();
            $table->string('status')->nullable();
            $table->string('using_product')->nullable();
            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('shade_card_id')->references('id')->on('shade_cards');
            $table->foreign('material_id')->references('id')->on('materials');
            $table->foreign('room_id')->references('id')->on('project_shade_rooms');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_shade_surfaces');
    }
}
