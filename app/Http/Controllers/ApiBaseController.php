<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiBaseController extends Controller
{
    public function sendResponse($data, $message = "Response success", $code = 200)
    {
        return response()->json([
            'success' => true,
            'data' => $data,
            'message' => $message,
        ], $code);
    }

    public function sendError($error, $code = 404)
    {
        return response()->json([
            'success' => false,
            'message' => $error,
        ], $code);
    }

    public function sendValidationError($error, $code=422)
    {
        return response()->json([
            'message' => 'The given data was invalid',
            'errors' => $error
        ], $code);
    }
}
