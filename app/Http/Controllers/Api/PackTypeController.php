<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CreatePackTypeRequest;
use App\Repositories\PackType\PackTypeRepository;

class PackTypeController extends ApiBaseController
{
    private $packType;

    public function __construct(PackTypeRepository $packType)
    {
        $this->packType = $packType;
    }

    public function index(Request $request)
    {
        return $this->sendResponse($this->packType->all($request->all()), 'Pack Types fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->packType->find($id), 'Single Pack Type retrieved successfully');
    }

    public function store(CreatePackTypeRequest $request)
    {
        return $this->sendResponse($this->packType->store($request->all()),'Pack Type saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->packType->update($request->all(), $id), 'Pack Type updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->packType->destroy($id), 'Pack Type deleted successfully');
    }
}
