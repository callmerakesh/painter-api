import $api from '../../handler/axios';
export const SET_RECEIPT = 'SET_RECEIPT';

const state = {
    receipts: {}
};

const mutations = {
    [SET_RECEIPT](status, payload) {
        state.receipts = payload
    },

};

const actions = {
    async getSingleReceipt(context, id) {
        let url = `/receipt/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllReceipts(context, payload) {
        let url =`/receipts/${payload.project_id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async addReceipt(context, formData) {
        let url = `/receipt`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteReceipt(context, id) {
        let url = `/receipt/${id}`;
        return await $api.delete(url);
    },
    async transferProjectProducts(context, formData){
        let url = `transfer-project-material`;
        let data = await $api.post(url, formData);
        return data.data;
    }
};

const getters  = {
    receipts : state => state.receipts,
};

export default {
    state,
    mutations,
    actions,
    getters
}
