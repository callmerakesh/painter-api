<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\CreateCustomerRequest;
use App\Repositories\Customers\CustomerRepository;
use Illuminate\Http\Request;

class CustomerController extends ApiBaseController
{
    private $customer;

    public function __construct(CustomerRepository $customer)
    {
        $this->customer = $customer;
    }

    public function index(Request $request)

    {
        return $this->sendResponse($this->customer->all($request->all()), 'Customers fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->customer->find($id), 'Single Customer retrieved successfully');
    }

    public function store(CreateCustomerRequest $request)
    {
        return $this->sendResponse($this->customer->store($request->all()),'Customer saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->customer->update($request->all(), $id), 'Customer updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->customer->destroy($id), 'Customer deleted successfully');
    }


}
