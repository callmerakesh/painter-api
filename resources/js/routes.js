import Dashboard from "./pages/Dashboard";
import Employee from "./pages/Employee";
import Category from "./pages/Category";
import Product from "./pages/Product";
import Referral from "./pages/Referral";
import Customer from "./pages/Customer";
import Vendor from "./pages/Vendor";
import Material from "./pages/Material";
import Project from "./pages/Project";
import ProjectProfile from "./pages/ProjectProfile";
import Unit from "./pages/Unit";
import EmployeeProfile from "./pages/EmployeeProfile";
import Stock from "./pages/Stock";
import EquipmentCategory from "./pages/EquipmentCategory";
import Equipment from "./pages/Equipment";
import Process from "./pages/Process";
import Attendance from "./pages/Attendacne";
import Contract from "./pages/Contract";
import Manufacture from "./pages/Manufacture";
import Shade from "./pages/Shade";
import ShadePalette from "./pages/ShadePalette";
import AttendanceList from "./pages/AttendanceList";

export default [
    {
        path:"/dashboard",
        component: Dashboard,
        name: 'dashboard',
        beforeEnter: (to, from, next) => {
            next();
        }
    },
    {
        path: "/employees",
        component: Employee,
        name: 'employees',
        beforeEnter: (to, from, next) => {
            next();
        }
    },
    {
        path: "/employee/:id",
        component: EmployeeProfile,
        name: "employeeProfile",
        beforeEnter: (to, from , next) => {
            next();
        }
    },
    {
        path:"/category",
        component: Category,
        name: 'category',
        beforeEnter:(to, from, next) => {
            next();
        }
    },
    {
        path: "/product",
        component: Product,
        name: 'product',
        beforeEnter:(to, from, next) => {
            next();
        }
    },
    {
        path: "/process",
        component: Process,
        name: 'process',
        beforeEnter:(to, from, next) => {
            next();
        }
    },
    {
        path: "/referral",
        component: Referral,
        name: 'referral',
        beforeEnter:(to, from, next) => {
            next();
        }
    },
    {
        path: "/customer",
        component: Customer,
        name: 'customer',
        beforeEnter:(to, from, next) => {
            next();
        }
    },
    {
        path: "/vendor",
        component: Vendor,
        name: 'vendor',
        beforeEnter:(to, from, next) => {
            next();
        }
    },
    {
        path: "/material",
        component: Material,
        name: 'material',
        beforeEnter:(to, from, next) => {
            next();
        }
    },
    {
        path: "/project",
        component: Project,
        name: 'project',
        beforeEnter: (to, from , next) => {
            next();
        }
    },
    {
        path: "/project/:project_id",
        component: ProjectProfile,
        name: 'projectProfile',
        beforeEnter: (to, from , next) => {
            next();
        }
    },
    {
        path: "/unit",
        component: Unit,
        name: 'unit',
        beforeEnter: (to, from, next) => {
            next();
        }
    },
    {
        path: "/contract",
        component: Contract,
        name: 'contract',
        beforeEnter: (to, from, next) => {
            next();
        }
    },
    {
        path: "/stock",
        component: Stock,
        name: 'stock',
        beforeEnter: (to, from, next) => {
            next();
        }
    },
    {
        path: "/equipment-category",
        component: EquipmentCategory,
        name: 'equipment-category',
        beforeEnter: (to, from, next) => {
            next();
        }
    },
    {
        path: "/equipment",
        component: Equipment,
        name: 'equipment',
        beforeEnter: (to, from, next) => {
            next();
        }
    },
    {
        path: "/attendance/:detail_id",
        component: Attendance,
        name: 'attendance',
        beforeEnter: (to, from, next) => {
            next();
        }
    },
    {
        path: "/manufacture",
        component: Manufacture,
        name: 'manufacture',
        beforeEnter: (to, from, next) => {
            next();
        }
    },
    {
        path: "/shade",
        component: Shade,
        name: 'shade',
        beforeEnter: (to, from, next) => {
            next();
        }
    },
    {
        path: "/shade-palette",
        component: ShadePalette,
        name: 'shade-palette',
        beforeEnter: (to, from, next) => {
            next();
        }
    },
    {
        path: "/attendance-list",
        component: AttendanceList,
        beforeEnter: (to, from, next) => {
            next();
        }
    },
    {
        path: "*",
        redirect: "/dashboard"
    }
]
