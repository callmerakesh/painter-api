<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectShadeRoomDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_shade_rooms', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('project_id')->nullable();
            $table->unsignedBigInteger('shade_card_id')->nullable();
            $table->string('room_name')->nullable();
            $table->string('alternative_room_name')->nullable();
            $table->string('room_position')->nullable();
            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('shade_card_id')->references('id')->on('shade_cards');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_shade_rooms');
    }
}
