<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vendors extends Model
{
    use HasFactory;

    protected $table = 'vendors';

    protected $guarded = ['id'];

    /*public function materials()
    {
        return $this->belongsToMany(Materials::class);
    }*/

    public function getCreatedAtAttribute($data)
    {
        return Carbon::parse($data)->format('Y-m-d');
    }
}
