<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class  Materials extends Model
{
    use HasFactory;

    protected $table = 'materials';

    protected $guarded = ['id'];

    public function category()
    {
        return $this->belongsTo(ProductTypes::class);
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function manufacture()
    {
        return $this->belongsTo(Manufacture::class);
    }

    public function alternativeUnits()
    {
        return $this->hasMany(MaterialAlternativeUnit::class,'material_id');
    }

    public function group()
    {
        return $this->belongsTo(MaterialGroup::class, 'material_group_id');
    }

    public function getCreatedAtAttribute($data)
    {
        return Carbon::parse($data)->format('Y-m-d');
    }
}
