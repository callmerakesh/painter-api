import Vue from 'vue';
import VueRouter from "vue-router";

Vue.use(VueRouter);

import painterRoutes from './routes';

window.user = JSON.parse(localStorage.getItem('user'));

let routes = painterRoutes;

const router = new VueRouter({
    mode:'history',
    routes
});

export default router;
