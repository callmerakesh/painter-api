<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use Illuminate\Http\Request;
use App\Http\Requests\CreateShadePaletteRequest;
use App\Repositories\ShadePalette\ShadePaletteRepository;

class ShadePaletteController extends ApiBaseController
{
    private $shadePalette;

    public function __construct(ShadePaletteRepository $shadePalette)
    {
        $this->shadePalette = $shadePalette;
    }

    public function index(Request $request)
    {
        return $this->sendResponse($this->shadePalette->all($request->all()), 'Shade Palettes fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->shadePalette->find($id), 'Single Shade Palette retrieved successfully');
    }

    public function store(Request $request)
    {
        return $this->sendResponse($this->shadePalette->store($request->all()),'Shade Palette saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->shadePalette->update($request->all(), $id), 'Shade Palette updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->shadePalette->destroy($id), 'Shade Palette deleted successfully');
    }
}
