<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Requisition extends Model
{
    use HasFactory;

    protected $table = 'requisition';

    protected $guarded = ['id'];

    public function material()
    {
        return $this->belongsTo(Materials::class, 'material_id');
    }
}
