import $api from '../../handler/axios';
export const SET_VENDORS = 'SET_VENDORS';

const state = {
    vendors: {}
};

const mutations = {
    [SET_VENDORS](status, payload) {
        state.vendors = payload
    },

};

const actions = {
    async getSingleVendor(context, id) {
        let url = `/vendor/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllVendors(context,payload) {
        let url =`/vendors?page=${payload.page}&search=${payload.search}&type=${payload.type}`;
        let data = await $api.get(url);
        return data.data;
    },
    async addVendor(context, formData) {
        let url = `/vendor`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteVendor(context, id) {
        let url = `/vendor/${id}`;
        return await $api.delete(url);
    }

};

const getters  = {
    vendors : state => state.vendors,
};

export default {
    state,
    mutations,
    actions,
    getters
}
