import $api from '../../handler/axios';
export const SET_PRODUCTS = 'SET_PRODUCTS';

const state = {
    products: {}
};

const mutations = {
    [SET_PRODUCTS](status, payload) {
        state.products = payload
    },

};

const actions = {
    async getSingleProduct(context, id) {
        let url = `/product/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllProducts(context,payload) {
        let url =`/products?page=${payload.page}&search=${payload.search}`;
        let data = await $api.get(url);
        return data.data;
    },
    async addProduct(context, formData) {
        let url = `/product`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteProduct(context, id) {
        let url = `/product/${id}`;
        return await $api.delete(url);
    },
    async getCategoryProducts(context, payload){
        let url = `/category-product/${payload.category_id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async syncProductProcess(context, formData) {
        let url = `/product/sync-process`;
        let data = await $api.post(url, formData);
        return data.data;
    }

};

const getters  = {
    products : state => state.products,
};

export default {
    state,
    mutations,
    actions,
    getters
}
