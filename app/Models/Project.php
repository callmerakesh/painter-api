<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $table = 'projects';

    protected $guarded = ['id'];

    protected $casts = ['is_active' => 'boolean'];

//    public function product()
//    {
//        return $this->belongsToMany(Products::class, 'project_product', 'project_id', 'product_id')->withPivot('product')->withTimestamps();
//    }

    public function customer()
    {
        return $this->belongsTo(Customers::class, 'customer_id');
    }

    public function referral()
    {
        return $this->belongsTo(Referrals::class, 'referral_id');
    }

    public function employee()
    {
        return $this->belongsTo(Employees::class, 'employee_id');
    }

    public function detail()
    {
        return $this->hasMany(ProjectDetail::class);
    }

    public function customerReferral()
    {
        return $this->belongsTo(Customers::class, 'customer_referral_id');
    }

    public function handOverDate()
    {
        return $this->hasMany(ProjectHandOverDate::class, 'project_id');
    }
}
