<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use Illuminate\Http\Request;
use App\Repositories\Process\ProcessRepository;
use App\Http\Requests\CreateProcessRequest;

class ProcessController extends ApiBaseController
{
    private $process;

    public function __construct(ProcessRepository $process)
    {
        $this->process = $process;
    }

    public function index(Request $request)
    {
        return $this->sendResponse($this->process->all($request->all()), 'All Process fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->process->find($id), 'Single Process retrieved successfully');
    }

    public function store(CreateProcessRequest $request)
    {
        return $this->sendResponse($this->process->store($request->all()),'Process saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->process->update($request->all(), $id), 'Process updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->process->destroy($id), 'Process deleted successfully');
    }
}
