import $api from '../../handler/axios';
export const SET_PROJECTS = 'SET_PROJECTS';

const state = {
    projects: {}
};

const mutations = {
    [SET_PROJECTS](status, payload) {
        state.projects = payload
    },

};

const actions = {
    async getSingleProject(context, id) {
        let url = `/project/${id}`;
        let data = await $api.get(url);
        // context.commit(SET_PROJECTS, data.data);
        return data.data;
    },
    async getAllProjects(context,payload) {
        let url =`/projects?page=${payload.page}&search=${payload.search}&status=${payload.status}`;
        let data = await $api.get(url);
        return data.data;
    },
    async addProject(context, formData) {
        let url = `/project`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteProject(context, id) {
        let url = `/project/${id}`;
        return await $api.delete(url);
    },
    async toggleProject(context, id) {
        let url = `/project/toggle/${id}`;
        let data = await $api.post(url);
        return data.data;
    },
    async getAllTransferProjects(context, id) {
        let url = `/transfer-project/${id}`;
        let data = await $api.get(url);
        return data.data;
    }

};

const getters  = {
    projects : state => state.projects,
};

export default {
    state,
    mutations,
    actions,
    getters
}
