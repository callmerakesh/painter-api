import $api from '../../handler/axios';
export const SET_REFERRALS = 'SET_REFERRALS';

const state = {
    referrals: {}
};

const mutations = {
    [SET_REFERRALS](status, payload) {
        state.referrals = payload
    },

};

const actions = {
    async getSingleReferral(context, id) {
        let url = `/referral/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllReferrals(context,payload) {
        let url =`/referrals?page=${payload.page}&search=${payload.search}&type=${payload.type}`;
        let data = await $api.get(url);
        return data.data;
    },
    async addReferral(context, formData) {
        let url = `/referral`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteReferral(context, id) {
        let url = `/referral/${id}`;
        return await $api.delete(url);
    }

};

const getters  = {
    referrals : state => state.referrals,
};

export default {
    state,
    mutations,
    actions,
    getters
}
