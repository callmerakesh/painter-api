import $api from '../../handler/axios';
export const SET_CUSTOMERS = 'SET_CUSTOMERS';

const state = {
    customers: {}
};

const mutations = {
    [SET_CUSTOMERS](status, payload) {
        state.customers = payload
    },

};

const actions = {
    async getSingleCustomer(context, id) {
        let url = `/customer/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllCustomers(context,payload) {
        let url =`/customers?page=${payload.page}&search=${payload.search}&type=${payload.type}`;
        let data = await $api.get(url);
        return data.data;
    },
    async addCustomer(context, formData) {
        let url = `/customer`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteCustomer(context, id) {
        let url = `/customer/${id}`;
        return await $api.delete(url);
    }

};

const getters  = {
    customers : state => state.customers,
};

export default {
    state,
    mutations,
    actions,
    getters
}
