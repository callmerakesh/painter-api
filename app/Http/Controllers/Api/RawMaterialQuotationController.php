<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\RawMaterialQuotation\RawMaterialQuotationRepository;

class RawMaterialQuotationController extends ApiBaseController
{
    public $rawMaterial;

    public function __construct(RawMaterialQuotationRepository $rawMaterial)
    {
        $this->rawMaterial = $rawMaterial;
    }

    public function index($projectId)
    {
        return $this->sendResponse($this->rawMaterial->all($projectId), 'Raw Materials fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->rawMaterial->find($id), 'Single Raw Material retrieved successfully');
    }

    public function store(Request $request)
    {
        return $this->sendResponse($this->rawMaterial->store($request->all()),'Raw Material saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->rawMaterial->update($request->all(), $id), 'Raw Material updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->rawMaterial->destroy($id), 'Raw Material deleted successfully');
    }
}
