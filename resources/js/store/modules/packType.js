import $api from '../../handler/axios';
export const SET_PACK_TYPES = 'SET_PACK_TYPES';

const state = {
    packTypes: {}
};

const mutations = {
    [SET_PACK_TYPES](status, payload) {
        state.packTypes = payload
    },

};

const actions = {
    async getSinglePackType(context, id) {
        let url = `/packType/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllPackTypes(context) {
        let url =`/packTypes`;
        let data = await $api.get(url);
        return data.data;
    },
    async addPackType(context, formData) {
        let url = `/packType`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deletePackType(context, id) {
        let url = `/packType/${id}`;
        return await $api.delete(url);
    }
};

const getters  = {
    packTypes : state => state.packTypes,
};

export default {
    state,
    mutations,
    actions,
    getters
}
