import $api from '../../handler/axios';
export const SET_RAW_MATERIAL_QUOTATION = 'SET_RAW_MATERIAL_QUOTATION';

const state = {
    rawMaterialQuotations: {}
};

const mutations = {
    [SET_RAW_MATERIAL_QUOTATION](status, payload) {
        state.rawMaterialQuotations = payload
    },

};

const actions = {
    async getSingleRawMaterialQuotation(context, id) {
        let url = `/raw-material/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllRawMaterialQuotations(context, payload) {
        let url =`/raw-materials/${payload.project_id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async addRawMaterialQuotation(context, formData) {
        let url = `/raw-material`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteRawMaterialQuotation(context, id) {
        let url = `/raw-material/${id}`;
        return await $api.delete(url);
    }
};

const getters  = {
    rawMaterialQuotations : state => state.rawMaterialQuotations,
};

export default {
    state,
    mutations,
    actions,
    getters
}
