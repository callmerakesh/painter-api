<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShadePaletteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shade_palette', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('shade_id')->nullable();
            $table->string('shade_name')->nullable();
            $table->string('shade_code')->nullable();
            $table->string('page_no')->nullable();
            $table->foreign('shade_id')->references('id')->on('shade_cards')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shade_palette');
    }
}
