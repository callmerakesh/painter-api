<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
//        dd($request->all());
        $user = Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')]);
        if($user) {
            return redirect('/dashboard');
        }
        $message = "Email or Passsword do not match";
        session()->flash('error', $message);
        return back();
    }

    public function logout()
    {
        auth()->logout();
        return redirect('/');
    }
}
