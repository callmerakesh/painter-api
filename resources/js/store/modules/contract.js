import $api from '../../handler/axios';
export const SET_CONTRACTS = 'SET_CONTRACTS';

const state = {
    contracts: {}
};

const mutations = {
    [SET_CONTRACTS](status, payload) {
        state.contracts = payload
    },

};

const actions = {
    async getSingleContract(context, id) {
        let url = `/contract/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllContracts(context) {
        let url =`/contracts`;
        let data = await $api.get(url);
        return data.data;
    },
    async addContract(context, formData) {
        let url = `/contract`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteContract(context, id) {
        let url = `/contract/${id}`;
        return await $api.delete(url);
    }
};

const getters  = {
    contracts : state => state.contracts,
};

export default {
    state,
    mutations,
    actions,
    getters
}
