<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\CreateProjectShadeRoomRequest;
use Illuminate\Http\Request;
use App\Repositories\ProjectShadeRoom\ProjectShadeRoomRepository;

class ProjectShadeRoomController extends ApiBaseController
{
    private $projectShadeRoom;

    public function __construct(ProjectShadeRoomRepository $projectShadeRoom)
    {
        $this->projectShadeRoom = $projectShadeRoom;
    }

    public function index($projectId)
    {
        return $this->sendResponse($this->projectShadeRoom->all($projectId), 'ProjectShadeRooms fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->projectShadeRoom->find($id), 'Single ProjectShadeRoom retrieved successfully');
    }

    public function store(Request $request)
    {
        return $this->sendResponse($this->projectShadeRoom->store($request->all()),'ProjectShadeRoom saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->projectShadeRoom->update($request->all(), $id), 'ProjectShadeRoom updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->projectShadeRoom->destroy($id), 'ProjectShadeRoom deleted successfully');
    }
}
