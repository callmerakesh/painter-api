<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\CreateEmployeeRequest;
use App\Repositories\Employees\EmployeeRepository;
use Illuminate\Http\Request;

class EmployeesController extends ApiBaseController
{
    private $employee;

    public function __construct(EmployeeRepository $employee)
    {
        $this->employee = $employee;
    }

    public function index(Request $request)
    {
        return $this->sendResponse($this->employee->all($request->all()), 'Employees fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->employee->find($id), 'Single Employee retrieved successfully');
    }

    public function store(CreateEmployeeRequest $request)
    {
        return $this->sendResponse($this->employee->store($request->all()),'Employee saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->employee->update($request->all(), $id), 'Employee updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->employee->destroy($id), 'Employee deleted successfully');
    }

    public function saveProfileImage(Request $request)
    {
        return $this->sendResponse($this->employee->saveProfileImage($request->all()), 'Profile Image Saved Successfully');
    }

    public function toggleEmployee(Request $request, $id)
    {
        return $this->sendResponse($this->employee->toggleEmployee($id), 'Employee toggled Successfully');
    }
}
