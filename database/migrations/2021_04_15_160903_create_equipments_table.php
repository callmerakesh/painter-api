<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEquipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipment_table', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->string('manufacture')->nullable();
            $table->string('variant')->nullable();
            $table->string('model_no')->nullable();
            $table->string('part_no')->nullable();
            $table->date('date_of_used')->nullable();
            $table->string('code')->nullable();
            $table->string('expected_life')->nullable();
            $table->double('purchase_amount')->nullable();
            $table->double('operating_cost')->nullable();
            $table->foreign('category_id')->references('id')->on('equipment_category');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipment_table');
    }
}
