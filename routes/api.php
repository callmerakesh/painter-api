<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'Api'], function() {
    Route::get('/employees',[\App\Http\Controllers\Api\EmployeesController::class, 'index']);
    Route::post('/save-profile', [\App\Http\Controllers\Api\EmployeesController::class, 'saveProfileImage']);
    Route::group(['prefix' => 'employee'], function() {
        Route::get('/{id}', [\App\Http\Controllers\Api\EmployeesController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\EmployeesController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\EmployeesController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\EmployeesController::class, 'destroy']);
        Route::post('/toggle/{id}',[\App\Http\Controllers\Api\EmployeesController::class, 'toggleEmployee']);
    });
    Route::get('/categories',[\App\Http\Controllers\Api\ProductTypeController::class, 'index']);
    Route::group(['prefix' => 'category'], function() {
        Route::get('/{id}', [\App\Http\Controllers\Api\ProductTypeController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\ProductTypeController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\ProductTypeController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\ProductTypeController::class, 'destroy']);

    });

    Route::get('/products',[\App\Http\Controllers\Api\ProductController::class, 'index']);
    Route::get('/category-product/{categoryId}', [\App\Http\Controllers\Api\ProductController::class, 'getCategoryProduct']);
    Route::post('product/sync-process', [\App\Http\Controllers\Api\ProductController::class, 'syncProcess']);

    Route::group(['prefix' => 'product'], function() {
        Route::get('/{id}', [\App\Http\Controllers\Api\ProductController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\ProductController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\ProductController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\ProductController::class, 'destroy']);
    });

    Route::get('/referrals',[\App\Http\Controllers\Api\ReferralController::class, 'index']);
    Route::group(['prefix' => 'referral'], function() {
        Route::get('/{id}', [\App\Http\Controllers\Api\ReferralController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\ReferralController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\ReferralController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\ReferralController::class, 'destroy']);

    });

    Route::get('/customers',[\App\Http\Controllers\Api\CustomerController::class, 'index']);
    Route::group(['prefix' => 'customer'], function() {
        Route::get('/{id}', [\App\Http\Controllers\Api\CustomerController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\CustomerController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\CustomerController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\CustomerController::class, 'destroy']);

    });

    Route::get('/vendors',[\App\Http\Controllers\Api\VendorController::class, 'index']);
    Route::group(['prefix' => 'vendor'], function() {
        Route::get('/{id}', [\App\Http\Controllers\Api\VendorController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\VendorController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\VendorController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\VendorController::class, 'destroy']);

    });
    Route::get('/materials',[\App\Http\Controllers\Api\MaterialController::class, 'index']);
    Route::group(['prefix' => 'material'], function() {
        Route::get('/{id}', [\App\Http\Controllers\Api\MaterialController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\MaterialController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\MaterialController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\MaterialController::class, 'destroy']);

    });

    Route::get('/projects', [\App\Http\Controllers\Api\ProjectController::class, 'index']);
    Route::group(['prefix' => 'project'], function(){
        Route::get('/{id}', [\App\Http\Controllers\Api\ProjectController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\ProjectController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\ProjectController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\ProjectController::class, 'destroy']);
        Route::post('/toggle/{id}',[\App\Http\Controllers\Api\ProjectController::class, 'toggleProject']);
    });

    Route::get('/project-details/{id}', [\App\Http\Controllers\Api\ProjectDetailController::class, 'index']);
    Route::group(['prefix' => 'project-detail'], function(){
        Route::get('/{id}', [\App\Http\Controllers\Api\ProjectDetailController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\ProjectDetailController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\ProjectDetailController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\ProjectDetailController::class, 'destroy']);
    });

    Route::get('/units', [\App\Http\Controllers\Api\UnitController::class, 'index']);
    Route::group(['prefix' => 'unit'], function(){
        Route::get('/{id}', [\App\Http\Controllers\Api\UnitController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\UnitController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\UnitController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\UnitController::class, 'destroy']);
    });

    Route::get('/equipment-categories', [\App\Http\Controllers\Api\EquipmentCategoryController::class, 'index']);
    Route::group(['prefix' => 'equipment-category'], function(){
        Route::get('/{id}', [\App\Http\Controllers\Api\EquipmentCategoryController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\EquipmentCategoryController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\EquipmentCategoryController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\EquipmentCategoryController::class, 'destroy']);
    });

    Route::get('/equipments', [\App\Http\Controllers\Api\EquipmentController::class, 'index']);
    Route::group(['prefix' => 'equipment'], function(){
        Route::get('/{id}', [\App\Http\Controllers\Api\EquipmentController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\EquipmentController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\EquipmentController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\EquipmentController::class, 'destroy']);
    });

    Route::get('/stocks', [\App\Http\Controllers\Api\StockController::class, 'index']);
    Route::group(['prefix' => 'stock'], function(){
        Route::get('/{id}', [\App\Http\Controllers\Api\StockController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\StockController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\StockController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\StockController::class, 'destroy']);
    });

    Route::get('/processes', [\App\Http\Controllers\Api\ProcessController::class, 'index']);
    Route::group(['prefix' => 'process'], function(){
        Route::get('/{id}', [\App\Http\Controllers\Api\ProcessController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\ProcessController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\ProcessController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\ProcessController::class, 'destroy']);
    });

    Route::get('/all-attendance/{detailId}', [\App\Http\Controllers\Api\AttendanceController::class, 'index']);
    Route::get('today-attendance',[\App\Http\Controllers\Api\AttendanceController::class, 'getTodayAttendance']);
    Route::get('today-absent-attendance',[\App\Http\Controllers\Api\AttendanceController::class, 'getTodayAbsentAttendance']);
    Route::post('/first-phase-attendance', [\App\Http\Controllers\Api\AttendanceController::class, 'storeFirstPhaseAttendance']);
    Route::post('/edit-first-phase-attendance', [\App\Http\Controllers\Api\AttendanceController::class, 'editFirstPhaseAttendance']);
    Route::post('/phase-attendance-job-end', [\App\Http\Controllers\Api\AttendanceController::class, 'addPhaseAttendanceJobEnd']);
    Route::group(['prefix' => 'attendance'], function(){
        Route::get('/{id}', [\App\Http\Controllers\Api\AttendanceController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\AttendanceController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\AttendanceController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\AttendanceController::class, 'destroy']);
    });


    Route::get('/contracts', [\App\Http\Controllers\Api\ContractController::class, 'index']);
    Route::group(['prefix' => 'contract'], function(){
        Route::get('/{id}', [\App\Http\Controllers\Api\ContractController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\ContractController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\ContractController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\ContractController::class, 'destroy']);
    });


    Route::get('/manufactures', [\App\Http\Controllers\Api\ManufactureController::class, 'index']);
    Route::group(['prefix' => 'manufacture'], function(){
        Route::get('/{id}', [\App\Http\Controllers\Api\ManufactureController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\ManufactureController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\ManufactureController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\ManufactureController::class, 'destroy']);
    });

    Route::get('/shades', [\App\Http\Controllers\Api\ShadeCardController::class, 'index']);
    Route::group(['prefix' => 'shade'], function(){
        Route::get('/{id}', [\App\Http\Controllers\Api\ShadeCardController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\ShadeCardController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\ShadeCardController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\ShadeCardController::class, 'destroy']);
    });

    Route::get('/shade-palettes', [\App\Http\Controllers\Api\ShadePaletteController::class, 'index']);
    Route::group(['prefix' => 'shade-palette'], function(){
        Route::get('/{id}', [\App\Http\Controllers\Api\ShadePaletteController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\ShadePaletteController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\ShadePaletteController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\ShadePaletteController::class, 'destroy']);
    });

    Route::get('/project-shade-rooms/{projectId}', [\App\Http\Controllers\Api\ProjectShadeRoomController::class, 'index']);
    Route::group(['prefix' => 'project-shade-room'], function(){
        Route::get('/{id}', [\App\Http\Controllers\Api\ProjectShadeRoomController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\ProjectShadeRoomController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\ProjectShadeRoomController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\ProjectShadeRoomController::class, 'destroy']);
    });

    Route::get('/project-shade-surfaces/{projectId}', [\App\Http\Controllers\Api\ProjectShadeSurfaceController::class, 'index']);
    Route::group(['prefix' => 'project-shade-surface'], function(){
        Route::get('/{id}', [\App\Http\Controllers\Api\ProjectShadeSurfaceController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\ProjectShadeSurfaceController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\ProjectShadeSurfaceController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\ProjectShadeSurfaceController::class, 'destroy']);
    });

    Route::get('/packTypes', [\App\Http\Controllers\Api\PackTypeController::class, 'index']);
    Route::group(['prefix' => 'packType'], function(){
        Route::get('/{id}', [\App\Http\Controllers\Api\PackTypeController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\PackTypeController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\PackTypeController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\PackTypeController::class, 'destroy']);
    });

    Route::get('/material-names', [\App\Http\Controllers\Api\MaterialGroupController::class, 'index']);
    Route::group(['prefix' => 'material-name'], function(){
        Route::get('/{id}', [\App\Http\Controllers\Api\MaterialGroupController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\MaterialGroupController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\MaterialGroupController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\MaterialGroupController::class, 'destroy']);
    });

    Route::get('/raw-materials/{projectId}', [\App\Http\Controllers\Api\RawMaterialQuotationController::class, 'index']);
    Route::group(['prefix' => 'raw-material'], function(){
        Route::get('/{id}', [\App\Http\Controllers\Api\RawMaterialQuotationController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\RawMaterialQuotationController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\RawMaterialQuotationController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\RawMaterialQuotationController::class, 'destroy']);
    });

    Route::get('/requisitions/{projectId}', [\App\Http\Controllers\Api\RequisitionController::class, 'index']);
    Route::group(['prefix' => 'requisition'], function(){
        Route::get('/{id}', [\App\Http\Controllers\Api\RequisitionController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\RequisitionController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\RequisitionController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\RequisitionController::class, 'destroy']);
    });

    Route::get('/purchase-orders/{projectId}', [\App\Http\Controllers\Api\PurchaseOrderController::class, 'index']);
    Route::group(['prefix' => 'purchase-order'], function(){
        Route::get('/{id}', [\App\Http\Controllers\Api\PurchaseOrderController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\PurchaseOrderController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\PurchaseOrderController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\PurchaseOrderController::class, 'destroy']);
    });
    Route::get('/receipts/{projectId}', [\App\Http\Controllers\Api\ReceiptController::class, 'index']);
    Route::group(['prefix' => 'receipt'], function(){
        Route::get('/{id}', [\App\Http\Controllers\Api\ReceiptController::class, 'show']);
        Route::post('/', [\App\Http\Controllers\Api\ReceiptController::class, 'store']);
        Route::post('/{id}', [\App\Http\Controllers\Api\ReceiptController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\ReceiptController::class, 'destroy']);
    });


    //transfer material from one project to another
    Route::get('/transfer-material/{id}', [\App\Http\Controllers\Api\MaterialController::class, 'getTransferMaterials']);
//    Route::get('/transfer-project/{id}', [\App\Http\Controllers\Api\ProjectController::class, 'getTransferProjects']);
    Route::post('transfer-project-material', [\App\Http\Controllers\Api\ReceiptController::class, 'transferMaterialFromProject']);

});

Route::get('/logout', function () {
    auth()->logout();
});
