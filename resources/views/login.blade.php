<html lang="en">
<head>
    <title>Welcome To Ultimate EX-IN painting solution</title>
{{--    @if(env('APP_ENV') == 'production')--}}
{{--        <link rel="stylesheet" type="text/css" href="{{ asset('painter-api/public/css/login.css') }}">--}}
{{--    @else--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/login.css') }}">
{{--     @endif--}}
</head>
<body>
<div class="form-wrapper">
    <div class="form-header">
        <span>Welcome To Ultimate EX-IN painting solution </span>
    </div>
    @if(session()->has('error'))
        <span class="session-msg">{{ session()->get('error') }}</span>
    @endif.
    <form method="POST" action="{{ route('user.login') }}">
        @csrf
        <div class="form-input-wrapper">
            <input type="email" name="email" class="form-control" placeholder="  Enter Email" required/>
            <input type="password" name = "password"  placeholder="  Enter Password" required>
            <input type="submit" value="Sign In">
        </div>
    </form>
</div>
</body>
</html>





