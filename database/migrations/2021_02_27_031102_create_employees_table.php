<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->date('date_of_join')->nullable();
            $table->string('employee_code')->nullable();
            $table->boolean('is_active')->default(1);
            $table->string('citizenship_name')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('father_name')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('grand_father_name')->nullable();
            $table->string('citizenship_no')->nullable();
            $table->dateTime('issued_date')->nullable();
            $table->string('issued_palace')->nullable();
            $table->string('address')->nullable();
            $table->string('state')->nullable();
            $table->string('district')->nullable();
            $table->string('municipality')->nullable();
            $table->string('ward_no')->nullable();
            $table->string('pan_no')->nullable();
            $table->string('bank_acc_no')->nullable();
            $table->string('designation')->nullable();
            $table->text('description')->nullable();
            $table->text('work_experience')->nullable();
            $table->text('management_skills')->nullable();
            $table->string('license_no')->nullable();
            $table->dateTime('license_issued_date')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('bank_branch')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('profile_image_path')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
