import $api from '../../handler/axios';
export const SET_EMPLOYEES = 'SET_EMPLOYEES';

const state = {
    employees: {}
};

const mutations = {
    [SET_EMPLOYEES](status, payload) {
        state.employees = payload
    },

};

const actions = {
    async getSingleEmployee(context, id) {
        let url = `/employee/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllEmployees(context, payload) {
        let url =`/employees?page=${payload.page}&search=${payload.search}&type=${payload.type}`;
        let data = await $api.get(url);
        return data.data;
    },
    async addEmployee(context, formData) {
        let url = `/employee`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteEmployee(context, id) {
        let url = `/employee/${id}`;
        return await $api.delete(url);
    },
    async toggleActive(context, id) {
        let url = `/employee/toggle/${id}`;
        let data = await $api.post(url);
        return data.data;
    }

};

const getters  = {
    employees : state => state.employees,
};

export default {
    state,
    mutations,
    actions,
    getters
}
