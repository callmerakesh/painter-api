<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\EquipmentRequest;
use Illuminate\Http\Request;
use App\Repositories\Equipment\EquipmentRepository;

class EquipmentController extends ApiBaseController
{
    private $equipment;

    public function __construct(EquipmentRepository $equipment)
    {
        $this->equipment = $equipment;
    }

    public function index(Request $request)
    {
        return $this->sendResponse($this->equipment->all($request->all()), 'Equipment fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->equipment->find($id), 'Single Equipment retrieved successfully');
    }

    public function store(EquipmentRequest $request)
    {
        return $this->sendResponse($this->equipment->store($request->all()),'Equipment saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->equipment->update($request->all(), $id), 'Equipment updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->equipment->destroy($id), 'Equipment deleted successfully');
    }
}
