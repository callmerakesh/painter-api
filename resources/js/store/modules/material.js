import $api from '../../handler/axios';
export const SET_MATERIALS = 'SET_MATERIALS';

const state = {
    materials: {}
};

const mutations = {
    [SET_MATERIALS](status, payload) {
        state.materials = payload
    },

};

const actions = {
    async getSingleMaterial(context, id) {
        let url = `/material/${id}`;
        let data = await $api.get(url);
        return data.data;
    },
    async getAllMaterials(context,payload) {
        let url =`/materials`;
        let data = await $api.get(url);
        return data.data;
    },
    async addMaterial(context, formData) {
        let url = `/material`;
        let data = await $api.post(url, formData);
        return data.data;
    },
    async deleteMaterial(context, id) {
        let url = `/material/${id}`;
        return await $api.delete(url);
    },

    async getAllTransferMaterials(context, id) {
        let url = `/transfer-material/${id}`;
        let data = await $api.get(url);
        return data.data;
    }

};

const getters  = {
    materials : state => state.materials,
};

export default {
    state,
    mutations,
    actions,
    getters
}
