<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Requisition\RequisitionRepository;

class RequisitionController extends ApiBaseController
{
    public $requisition;

    public function __construct(RequisitionRepository $requisition)
    {
        $this->requisition = $requisition;
    }

    public function index($projectId)
    {
        return $this->sendResponse($this->requisition->all($projectId), 'Requisitions fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->requisition->find($id), 'Single Requisition retrieved successfully');
    }

    public function store(Request $request)
    {
        return $this->sendResponse($this->requisition->store($request->all()),'Requisition saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->requisition->update($request->all(), $id), 'Requisition updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->requisition->destroy($id), 'Requisition deleted successfully');
    }
}
