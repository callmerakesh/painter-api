<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeResignDate extends Model
{
    use HasFactory;

    protected $table = 'employee_resign';

    protected $guarded = ['id'];

    public function employee()
    {
        return $this->belongsTo(Employees::class);
    }
}
