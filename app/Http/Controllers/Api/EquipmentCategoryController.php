<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\EquipmentCategoryRequest;
use Illuminate\Http\Request;
use App\Repositories\EquipmentCategory\EquipmentCategoryRepository;


class EquipmentCategoryController extends ApiBaseController
{
    private $category;

    public function __construct(EquipmentCategoryRepository $category)
    {
        $this->category = $category;
    }

    public function index(Request $request)
    {
        return $this->sendResponse($this->category->all($request->all()), 'Equipment Categories fetched successfully');
    }

    public function show($id)
    {
        return $this->sendResponse($this->category->find($id), 'Single Equipment Category retrieved successfully');
    }

    public function store(EquipmentCategoryRequest $request)
    {
        return $this->sendResponse($this->category->store($request->all()),'Equipment Category saved successfully');
    }

    public function update(Request $request, $id)
    {
        return $this->sendResponse($this->category->update($request->all(), $id), 'Equipment Category updated successfully');
    }

    public function destroy($id)
    {
        return $this->sendResponse($this->category->destroy($id), 'Equipment Category deleted successfully');
    }
}
