import Vue from 'vue';
import Vuex from 'vuex';

import Validation from './modules/validation';
import Loader from './modules/loader';
import Employee from './modules/employee';
import Auth from './modules/auth';
import Category from './modules/category';
import Product from './modules/product';
import Referral from './modules/referral';
import Customer from './modules/customer';
import Vendor from './modules/vendor';
import Material from './modules/material';
import Project from "./modules/project";
import ProjectWorkDetails from "./modules/project_work_details";
import Unit from "./modules/unit";
import EquipmentCategory from "./modules/equipmentCategory";
import Equipment from "./modules/equipment";
import Stock from "./modules/stock";
import Process from "./modules/process";
import Attendance from "./modules/attendance";
import Contract from "./modules/contract";
import Manufacture from "./modules/manufacture";
import Shade from "./modules/shade";
import RoomShade from "./modules/roomShade"
import SurfaceShade from "./modules/surfaceShade"
import PackType from "./modules/packType"
import MaterialName from "./modules/materialName"
import ShadePalette from "./modules/shadePalette"
import RawMaterialQuotation from "./modules/rawMaterialQuotation"
import Requisition from "./modules/requisition"
import PurchaseOrder from "./modules/purchaseOrder"
import Receipt from "./modules/receipt"
Vue.use(Vuex);

export default new Vuex.Store({

    modules: {
        Validation,
        Loader,
        Employee,
        Auth,
        Category,
        Product,
        Referral,
        Customer,
        Vendor,
        Material,
        Project,
        ProjectWorkDetails,
        Unit,
        EquipmentCategory,
        Equipment,
        Stock,
        Process,
        Attendance,
        Contract,
        Manufacture,
        Shade,
        RoomShade,
        SurfaceShade,
        PackType,
        MaterialName,
        ShadePalette,
        RawMaterialQuotation,
        Requisition,
        PurchaseOrder,
        Receipt
    },
    strict: false
});

